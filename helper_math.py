import math

PI = math.pi
TWOPI = 2 * PI
SQRTPI = math.sqrt(PI)
EEULER = math.e

def add(a, b):
    return a + b

def sub(a, b):
    return a - b

def mul(a, b):
    return a * b

def div(a, b):
    return a / b

