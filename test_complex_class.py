"""Implement unit test for the Complex class."""

from math import isclose, sqrt
from hypothesis import given
from hypothesis.strategies import integers, floats, tuples

from complex_class import Complex

VALUE_MIN, VALUE_MAX = -100, 100
cint_tuple1 = tuples(
    integers(min_value=VALUE_MIN, max_value=VALUE_MAX),
    integers(min_value=VALUE_MIN, max_value=VALUE_MAX),
)
cint_tuple2 = tuples(
    integers(min_value=VALUE_MIN, max_value=VALUE_MAX),
    integers(min_value=VALUE_MIN, max_value=VALUE_MAX),
)


@given(reim=cint_tuple1)
def test_init(reim):
    re, im = reim
    z = Complex(re, im)
    assert isclose(float(re), z.real)
    assert isclose(float(im), z.imag)
