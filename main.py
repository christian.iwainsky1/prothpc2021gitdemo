import helper_math as hm

if __name__ == "__main__":
    a, b = 3, 4
    print(hm.add(a, b))
    print(hm.sub(b, a))
    print(hm.mul(a, b))
    print(hm.div(a, b))
    print(hm.PI, hm.TWOPI, hm.EEULER)

