"""Implement a class for complex numbers."""


class Complex:
    """Custom complex number class."""

    def __init__(self, real=0, imag=0):
        self.real, self.imag = self.__same_numeric_type(re=real, im=imag)

    @classmethod
    def make_Complex(cls, other):
        if (type(other) is int) or (type(other) is float):
            return cls(real=other, imag=0)
        elif type(other) is Complex:
            return other
        else:
            raise ValueError("Invalid type passed to Complex constructor.")

    def __same_numeric_type(self, re, im):
        """Assert that real and imag part are of the same numerical type.

        If different, meaning any of (real, imag) is of type float while
        the other is not convert both to floats.
        """
        try:
            re + 1, im + 1
        except TypeError:
            print(
                """Input values of real or imag part are
                   not of type 'int' or 'float'."""
            )
            raise
        return float(re), float(im)

    def __repr__(self):
        """Printing a complex number."""
        import math

        copysign = math.copysign(1, self.imag)
        sign = "-" if str(copysign)[0] == "-" else "+"
        return "(%s%s%sj)" % (self.real, sign, int(copysign) * self.imag)

    def abs(self):
        """Absolute value."""
        return (self.real ** 2 + self.imag ** 2) ** 0.5
